//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

#include <iostream>
#include "Array.h"

bool testArray(); //function to test Array class
bool testFunctions();
int max (int a, int b);
float fmax (float a, float b);
double dmax ( double a, double b);
template <typename Type>
Type maxTemp (const Type a, const Type b);
template <typename Type>
Type minTemp (const Type a, const Type b);
template <typename Type>
Type twice (const Type a, const Type b);
template <typename Type>
void print (Type a[], int size);

int main (int argc, const char* argv[])
{
    
    /*if (testArray() == true)
     {
     std::cout << "all Array tests passed\n";
     }
     else
     {
     std::cout << "Array tests failed\n";
     }
     
     return 0;*/
    testFunctions();
}

double dmax (double a, double b)
{
    if (a<b)
        return b;
    else
        return a;
}


int max (int a, int b)
{
    if (a<b)
        return b;
    else
        return a;
}

float fmax (float a, float b)
{
    if (a<b)
        return b;
    else
        return a;
}

template <typename Type>
Type maxTemp (const Type a, const Type b)
{
    if (a<b)
        return b;
    else
        return a;
}

template <typename Type>
Type minTemp (const Type a, const Type b)
{
    if (a<b)
        return a;
    else
        return b;
}

template <typename Type>
Type twice (const Type a, const Type b)
{
    return a+b;
}

template <typename Type>
void print (Type a[], int size)
{
    
    for (int i = 0; i<size; i++) {
        std::cout<<a[i]<<"\n";
    }
}

template <typename Type>
double getAverage (Type a[], int size)
{
    double total=0;
    for (int i = 0; i<size; i++) {
        total=total+a[i];
    }
    return total/size;
}

bool testFunctions()
{
    if (max(5, 10) != 10) {
        std::cout<<"Error with max function\n";
        return false;
    }
    
    if (fmax (5.698f, 10.087f) != 10.087f) {
        std::cout<<"Error with fmax function\n";
        return false;
    }
    
    if (dmax(5.698321, 10.087321) != 10.087321) {
        std::cout<<"Error with dmax function\n";
        return false;
    }
    
    if (maxTemp(2.103, 6.0872) != 6.0872) {
        std::cout<<"Error with maxTemp template\n";
    }
    
    if (minTemp(2.103, 6.0872) != 2.103) {
        std::cout<<"Error with minTemp template\n";
    }
    
    if (twice (2.103, 6.00) != 8.103) {
        std::cout<<"Error with twice template\n";
    }
    
    const float testArray[] = {0.54, 1.00, 2.09, 3.27, 4.73, 5.91};
    std::cout<<"Array values are:\n";
    print(testArray, 6);
    std::cout<<"And the mean value is: "<<getAverage(testArray, 6)<<"\n";
    
    std::cout<<"Sucess!"
    ;
    return true;
    
    
    
}

bool testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    //    //removing first
    //    array.remove (0);
    //    if (array.size() != testArraySize - 1)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i+1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //removing last
    //    array.remove (array.size() - 1);
    //    if (array.size() != testArraySize - 2)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i + 1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //remove second item
    //    array.remove (1);
    //    if (array.size() != testArraySize - 3)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    if (array.get (0) != testArray[1])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (1) != testArray[3])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (2) != testArray[4])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    
    return true;
}
